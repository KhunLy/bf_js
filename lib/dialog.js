const openConfirmDialog = (message) => {
    // ajout du backdrop
    const backdrop = createBackDrop();
    const promise = createDialog(backdrop, message);
    return promise;
}

const dismissBackdrop = (bd) => {
    return () => {
        document.querySelector('.dialog').classList.add('close');
        bd.classList.add('close');
        setTimeout(() => {
            document.body.removeChild(bd);
        }, 500);
    }
}

const createBackDrop = () => {
    const backdrop = document.createElement('div');
    backdrop.classList.add('backdrop');
    backdrop.onclick = dismissBackdrop(backdrop);
    document.body.appendChild(backdrop);
    return backdrop;
}

const createDialog = (bd, message) => {
    const dialog = document.createElement('div');
    dialog.classList.add('dialog');
    const h4 = document.createElement('h4');
    h4.innerText = message;
    dialog.appendChild(h4);

    const promise = createActions(dialog);
    bd.appendChild(dialog);

    return promise;
}

const createActions = (dialog) => {
    const div = document.createElement('div');
    div.style.textAlign = 'right';

    const okBtn = document.createElement('button');
    okBtn.classList.add('okBtn');
    okBtn.innerText = 'OK';
    div.appendChild(okBtn);

    const cancelBtn = document.createElement('button');
    cancelBtn.classList.add('cancelBtn');
    cancelBtn.innerText = 'Annuler';
    div.appendChild(cancelBtn);

    dialog.appendChild(div);

    return new Promise(resolve => {
        okBtn.onclick = () => resolve(true);
        cancelBtn.onclick = () => resolve(false);
    });
} 

