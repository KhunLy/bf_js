const spanTemp = document.getElementById('span_temp');
const imgIcon = document.getElementById('img_icon');

// let xhr = new XMLHttpRequest();

// const baseUrl 
//     = 'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={apikey}&units={unit}'

// const baseImgSrc = 'http://openweathermap.org/img/wn/{icon}@2x.png';
// // open(verbHttp, url)
// xhr.open('GET', 
//     baseUrl.replace('{lon}', 4.3)
//         .replace('{lat}', 50.8)
//         .replace('{apikey}', 'd52e50e34214ff0b92247f788638eeb9')
//         .replace('{unit}', 'metric')
// );

// // envoyer la requête
// xhr.send();


// xhr.onreadystatechange = e => {
//     // status = finished
//     if(xhr.readyState === 4) {
//         // deserialiser un text en objet
//         let result = JSON.parse(e.currentTarget.responseText);
//         spanTemp.innerText = result.main.temp;
//         imgIcon.src = baseImgSrc
//             .replace('{icon}', result.weather[0].icon);
//     }
// }

const baseUrl = 'https://api.openweathermap.org/data/2.5/weather';
const baseImgSrc = 'https://openweathermap.org/img/wn/{icon}@2x.png';

const params = {
    lat: 50.8,
    lon: 4.3,
    appid: 'd52e50e34214ff0b92247f788638eeb9',
    units: 'metric'
}

axios.get(baseUrl, { params }).then(({data}) => {
    spanTemp.innerText = data.main.temp;
    imgIcon.src = baseImgSrc.replace('{icon}', data.weather[0].icon);
});
