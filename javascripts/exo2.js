const articles = [];

const btn_ajouter = document.getElementById('btn_ajouter');
const input_article = document.getElementById('input_article');
const span_char = document.getElementById('span_char');
const ul = document.getElementById('ul_articles');

const afficherUneCollection = (ul, liste) => {
    ul.innerHTML = '';
    for(let a of liste) {
        ul.innerHTML += `<li>${a}</li>`;
    }
}

btn_ajouter.onclick = () => {
    let nouvelArticle = input_article.value;
    if(nouvelArticle) {
        const index = articles.indexOf(nouvelArticle);
        if(index === -1) { // article non trouvé
            articles.push(nouvelArticle);
        }
        else {
            articles.splice(index, 1);
        }
    }
    span_char.innerHTML 
        = articles.reduce((prev, item) => prev + item.length ,0);
    afficherUneCollection(ul, articles);
}




