const load = () => {
    let string = localStorage.getItem('MY_PRODUCTS');
    return string ? JSON.parse(string) : [];
}

let produits = load();

const tbody = document.querySelector('tbody');
const inputNom = document.getElementById('input_nom');
const inputPrix = document.getElementById('input_prix');
const inputPromo = document.getElementById('input_promo');
const inputSubmit = document.getElementById('input_add');
const form = document.querySelector('form');

const save = () => {
    let string = JSON.stringify(produits);
    localStorage.setItem('MY_PRODUCTS', string);
}



const ajouterTd = (tr, value) => {
    const td = document.createElement('td');
    td.innerText = value;
    tr.appendChild(td);
}

const deleteItem = (itemToRemove, id) => {
    return async () => {
        let result = await openConfirmDialog('Coucou');
        if(result) {
            tbody.removeChild(itemToRemove);
            produits = produits.filter(p => p.id !== id);
            save();
        }
    }
}

const calculPromo = (prix, promo) => 
    (prix - (prix * promo) / 100).toFixed(2);

const ajouterLigne = (item) => {
    const tr = document.createElement('tr');
    // colonne id
    ajouterTd(tr, item.id);
    // colonne nom
    ajouterTd(tr, item.nom);

    // colonne prix
    const tdPrix = document.createElement('td');
    if(item.promo === 0) {
        tdPrix.innerText = item.prix + '€';
    } else {
        const span1 = document.createElement('span');
        span1.classList.add('prix_barre');
        span1.innerText = item.prix + '€';

        const span2 = document.createElement('span');
        span2.classList.add('prix_reduit');
        span2.innerText = calculPromo(item.prix, item.promo) + '€';

        tdPrix.appendChild(span1);
        tdPrix.appendChild(span2);
    }
    tr.appendChild(tdPrix);

    // colonne button supprimer
    const tdSup = document.createElement('td');
    const btnSup = document.createElement('button');
    btnSup.innerText = 'X';
    btnSup.onclick = deleteItem(tr, item.id);
    tdSup.appendChild(btnSup);
    tr.appendChild(tdSup);

    tbody.appendChild(tr);
}

for(let item of produits) {
    ajouterLigne(item);
}



inputSubmit.addEventListener('click', e => {
    e.preventDefault();

    if(form.checkValidity()) {
        let newP = {
            id: produits.map(p => p.id).length ? (Math.max(...produits.map(p => p.id)) + 1) : 1,
            nom: inputNom.value,
            prix: parseFloat(inputPrix.value),
            promo: parseInt(inputPromo.value) || 0
        }
        ajouterLigne(newP);
        produits.push(newP);
        save();
    }
});


