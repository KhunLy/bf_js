const imgLarge = document.querySelector('.img-large');
// un HTMLElement
const imgsSmall = document.querySelectorAll('.img-small');
// collection HTMLElement

const clickHandler = ({target}) => { 
    // target = element sur lequel on vient de cliquer
    const imageSrc = target.src;
    //const imageSrc = target.getAttribute('src');
    imgLarge.src = imageSrc;
    //imageSrc.setAttribute('src', imageSrc);

    for(let img2 of imgsSmall) {
        img2.classList.remove('selected')
    }

    target.classList.add('selected');
}

for(let img of imgsSmall) {
    img.onclick = clickHandler;
}