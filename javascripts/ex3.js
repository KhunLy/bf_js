const btnSubmit  = document.getElementById('btn_submit');
const inputNom  = document.getElementById('input_nom');
const inputAge  = document.getElementById('input_age');
const inputVaccine  = document.getElementById('input_vaccine');
const inputEmploye  = document.getElementById('input_employe');
const form = document.querySelector('form');

const inputs = document.querySelectorAll('input');

for(let input of inputs) {
    //input.oninput = e => input.classList.add('touched');
    input.addEventListener('input', e => {
        input.classList.add('touched');
        if(form.checkValidity()) { 
            // retourne true  si tous les inputs sont valides 
            // ou false si au moins un input est invalide 
            btnSubmit.removeAttribute('disabled');
        } else {
            btnSubmit.setAttribute('disabled', true);
        }
    });
}

// input.addEventListener('click', e => {});
// input.addEventListener('change', e => {});
// input.addEventListener('input', e => {});
// input.addEventListener('focus', e => {});
// input.addEventListener('blur', e => {}); // perte du focus
// input.addEventListener('mouseover', e => {}); // survole un element
// input.addEventListener('mouseleave', e => {}); // ne survole plus
// input.addEventListener('keydown', e => {});
// input.addEventListener('keyup', e => {});<
// window.addEventListener('scroll', e => {});


btnSubmit.onclick = (e) => {
    // supprimer le comportement par défaut
    e.preventDefault();
    console.log(inputNom.value);
    console.log(parseInt(inputAge.value));
    console.log(inputVaccine.checked);
    console.log(new Date(inputEmploye.value));
}