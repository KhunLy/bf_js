const api_url = 'https://api.agify.io'; 
const country_api_url = 'https://restcountries.com/v3.1/all'; 

const inputName = document.getElementById('input_name');
const btnSend = document.getElementById('btn_send');
const spanAge = document.getElementById('span_age');
const loader = document.querySelector('.loader');
const select = document.getElementById('select_countries');

const loadCountries = async () => {
    const {data} = await axios.get(country_api_url);
    let options = data
        .filter(c => c.continents.includes('Europe'))
        .sort((c1, c2) => c1.translations.fra.common < c2.translations.fra.common ? -1 : 1)
        .map(c => {
            const option = document.createElement('option');
            option.innerText = c.translations.fra.common;
            option.setAttribute('value', c.cca2);
            return option;
        });
    select.append(...options);
}

loadCountries();

const clickHandler = async e => {
    e.preventDefault();
    let name = inputName.value;
    let country_id = select.value;
    let params = { name, country_id };

    btnSend.setAttribute('disabled', true);
    loader.classList.remove('hidden');
    try {
        const {data} = await axios.get(api_url, { params });
        spanAge.innerText = data.age;
    } catch {
        toastr.error('impossible de charger les données');
    } finally {
        loader.classList.add('hidden');
        btnSend.removeAttribute('disabled');
    }


    
    
}

btnSend.addEventListener('click', clickHandler);