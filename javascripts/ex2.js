// window l'objet window contient toute les infos de la fenetre
console.log(window.location.href);

// redirection vers une autre page
// window.location.href = 'http://localhost:5500/exo1.html';

// depuis window on a access à des "storages" qui permetttront 
// d'enregistrer des données
// ex : window.localStorage

// navigator.geolocation permet de récupérer votre position
// window.navigator.geolocation
    //.getCurrentPosition(({coords}) => console.log(coords))

// document l'ensemble des noeuds html

// selecteurs 

// recupérer la balise body
console.log(document.body);

// recuperer un element grace à son id // VERY BAD PRACTICE !!!!
// console.log(myId)
// recuperer un element grace à son id
console.log(document.getElementById('myId'));
//document.getElementById('myId').innerHTML = 'test';

// recuperer une collection de balise html (htmlCollection)
console.log(document.getElementsByTagName('a'));

// attention il faudra parcourir la collection pour pouvoir
// manipuler les éléments
// for(let item of document.getElementsByTagName('a')) {
//     item.innerHTML = 'test';
// }

// recupérer des elements grace à leur classe
console.log(document.getElementsByClassName('maClasse'));

// recuperer un element grace à un selecteur css
console.log(document.querySelector('ul.maListe>li:nth-child(4)'));
//document.querySelector('ul.maListe>li:nth-child(4)').innerHTML = "test"

// recuperer une collection d'elements grace à un selecteur css
console.log(document.querySelectorAll('ul.maListe>li'));

// htmlElement.value // recuperer la value d'un input

// htmlElement.addEventListener('click', fct) // ajouter un evenement click 

// htmlElement.onclick = fct; // ajouter un evenement click

// modifier l'html d'un element
// document.getElementById('myId').innerHTML = 'hello';

// modifier le texte d'un element
document.getElementById('myId').innerText = '<p>hello</p>'; 
// les balises vont apparaitre

document.getElementById('toggle_btn').onclick = () => {
    // // ajouter une classe
    // document.getElementById('myId').classList.add('hidden');
    // // supprimer une classe
    // document.getElementById('myId').classList.remove('hidden');
    // // ajouter ou supprimer une classe
    // document.getElementById('myId').classList.toggle('hidden');
    // recupérer la valeur d'un attribut
    // document.getElementById('toggle_btn').getAttribute('disabled');
    // ajouter un attribut
    // document.getElementById('toggle_btn').setAttribute('disabled', true);
    // retirer un attribut
    // document.getElementById('toggle_btn').removeAttribute('disabled');
    // modifier ou recuperer une valeur css
    document.getElementById('myId').style.color = 'red';

}


// afficher une grande image et 3 miniatures
// lorsque l'on clique sur une miniature 
// l'attribut src de la grande image doit changer
// la miniature sélectionnée sera encadrée en rouge


// const h1 = document.createElement('h1');
// h1.classList.add('red');
// h1.innerText = 'Ce paragraphe a été créé en javascript';

// document.getElementById('empty').appendChild(h1);
// setTimeout(() => {
//     document.getElementById('empty').removeChild(h1);
// }, 2000);

document.getElementById('empty').innerHTML 
    = '<h1 class="red">Ce paragraphe a été créé en javascript</h1>';


